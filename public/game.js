import { getFirestore, collection, addDoc, getDocs } from "https://www.gstatic.com/firebasejs/10.8.0/firebase-firestore.js";
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.8.0/firebase-app.js";

const firebaseConfig = {
    apiKey: "AIzaSyDM6wusxGT0G7VPW9-gtrLMzH-UhgdCJJc",
    authDomain: "smack-my-blocks-up.firebaseapp.com",
    projectId: "smack-my-blocks-up",
    storageBucket: "smack-my-blocks-up.appspot.com",
    messagingSenderId: "22580151338",
    appId: "1:22580151338:web:14502c7d8e4f18be026f53",
    measurementId: "G-7701VV59XE"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

class Entity {
    constructor(maxHealth, level) {
        this.maxHealth = maxHealth;
        this.health = maxHealth;
        this.level = level;
    }

    takeDamage(damage) {
        this.health -= damage;
        if (this.health <= 0) {
            this.die();
            return true;
        }
        return false;
    }

    die() {
        console.log('Entity has died');
    }
}

class Player extends Entity {
    constructor(maxHealth, level) {
        super(maxHealth, level);
    }

    heal(amount) {
        this.health += amount;
        if (this.health > this.maxHealth) {
            this.health = this.maxHealth;
        }
    }
}

class Enemy extends Entity {
    constructor(maxHealth, level) {
        super(maxHealth, level);
    }

    updateHealthDisplay(cell) {
        cell.style.opacity = `${this.health / this.maxHealth}`;
    }
}

let startTime, player, playerPosition, enemies, healPosition, gameInterval, cells = [], Name, score = 100, timerInterval;
const maxTime = 1000; // 1000 seconds for the game

function startGame() {
    const playerName = document.getElementById('playerName').value || 'Anonymous';
    
    startTime = Date.now();

    const grid = document.getElementById('grid');
    grid.innerHTML = '';
    cells = [];

    for (let i = 0; i < 100; i++) {
        let cell = document.createElement('div');
        cell.classList.add('cell');
        grid.appendChild(cell);
        cells.push(cell);
    }

    player = new Player(100, 1);
    playerPosition = 0;
    cells[playerPosition].classList.add('player');
    updatePlayerHealthBar(player.health);

    enemies = [];
    for (let i = 0; i < 10; i++) {
        let enemyPosition;
        do {
            enemyPosition = Math.floor(Math.random() * 100);
        } while (cells[enemyPosition].classList.contains('enemy'));
        
        let enemyLevel = Math.floor(Math.random() * 3) + 1;
        let enemy = new Enemy(50 * enemyLevel, enemyLevel);
        let enemyCell = cells[enemyPosition];
        enemyCell.classList.add('enemy');
        enemy.updateHealthDisplay(enemyCell);
        enemies.push({enemy, position: enemyPosition});
    }

    placeHealthBlock();

    if (timerInterval) clearInterval(timerInterval);
    timerInterval = setInterval(updateTimer, 1000);

    document.removeEventListener('keydown', handlePlayerMovement);
    document.addEventListener('keydown', handlePlayerMovement);
    getLeaderboardScores();
}

function placeHealthBlock() {
    let availableCells = cells.filter(cell => !cell.classList.contains('enemy') && !cell.classList.contains('player'));
    if (availableCells.length > 0) {
        let randomCell = availableCells[Math.floor(Math.random() * availableCells.length)];
        randomCell.classList.add('heal');
    }
}

function handlePlayerMovement(e) {
    let previousPosition = playerPosition;

    switch (e.key) {
        case 'ArrowUp': playerPosition = Math.max(playerPosition - 10, 0); break;
        case 'ArrowDown': playerPosition = Math.min(playerPosition + 10, 99); break;
        case 'ArrowLeft': playerPosition = playerPosition % 10 === 0 ? playerPosition : playerPosition - 1; break;
        case 'ArrowRight': playerPosition = playerPosition % 10 === 9 ? playerPosition : playerPosition + 1; break;
    }

    let enemyCollision = enemies.find(enemyObj => playerPosition === enemyObj.position);
    if (enemyCollision) {
        player.takeDamage(10);
        const isEnemyDefeated = enemyCollision.enemy.takeDamage(player.level * 10);
        enemyCollision.enemy.updateHealthDisplay(cells[enemyCollision.position]);
        if (isEnemyDefeated) {
            let enemyCell = cells[enemyCollision.position];
            enemyCell.classList.remove('enemy');
            enemyCell.style.opacity = ''; // Reset opacity for the cell
            enemies = enemies.filter(enemyObj => enemyObj !== enemyCollision);
        } else {
            playerPosition = previousPosition; // Stay in the same spot if enemy is not defeated
        }
    }

    cells[previousPosition].classList.remove('player');
    if (!cells[playerPosition].classList.contains('enemy')) {
        cells[playerPosition].classList.add('player');
    }

    if (cells[playerPosition].classList.contains('heal')) {
        player.heal(20);
        cells[playerPosition].classList.remove('heal');
        placeHealthBlock();
    }

    updatePlayerHealthBar(player.health);

    // Check game state
    checkGameState();
}

document.addEventListener('DOMContentLoaded', () => {
    const startGameButton = document.getElementById('startGameButton');
    if (startGameButton) {
        startGameButton.addEventListener('click', startGame);
    }

function updatePlayerHealthBar(health) {
    const healthBar = document.getElementById('health-bar');
    healthBar.style.width = `${health}%`;
}

function updateTimer() {
    const timerElement = document.getElementById('timer');
    const timeRemaining = maxTime - Math.floor((Date.now() - startTime) / 1000);

    timerElement.textContent = `Time Remaining: ${timeRemaining} seconds`;

    if (timeRemaining <= 0) {
        clearInterval(timerInterval);
        score = 0; // Set score to 0 if time runs out
        endGame();
    }
}

function endGame() {
    alert(`Game Over. Your score is ${score}. Press OK to restart.`);
    addScoreToLeaderboard(Name, score);
    startGame();
}

function checkGameState() {
    if (player.health <= 0) {
        score = 0; // Set score to 0 if player health reaches 0
        endGame();
    } else if (enemies.length === 0) {
        const timeTaken = Math.floor((Date.now() - startTime) / 1000);
        score = Math.max(0, 100 - Math.floor(timeTaken / 10)); // Calculate score based on time
        endGame();
    }
}


async function addScoreToLeaderboard(name, score) {
    try {
        const docRef = await firebase.firestore().collection("Leaderboard").add({
            name: name,
            score: score
        });
        console.log("Document written with ID: ", docRef.id);
    } catch (error) {
        console.error("Error adding document: ", error);
    }
}

async function getLeaderboardScores() {
    try {
        const querySnapshot = await firebase.firestore().collection("Leaderboard").get();
        querySnapshot.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data().name}: ${doc.data().score}`);
            // Update leaderboard display logic here
        });
    } catch (error) {
        console.error("Error getting documents: ", error);
    }
}



  
  

document.addEventListener('DOMContentLoaded', startGame);
